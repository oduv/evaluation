package com.analyzing.appl;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.analyzing.appl.domain.StatisticData;
import com.analyzing.appl.service.StatisticAnalyserService;

@SpringBootTest
class StatisticAnalyserServiceTests {
	@Autowired
	private StatisticAnalyserService objectUnderTest;
	
	@Test
	void filterCollectionForDesiredYear() {
		Calendar calendar2013=Calendar.getInstance();
		calendar2013.set(Calendar.YEAR, 2013);
		Calendar calendar2015=Calendar.getInstance();
		calendar2015.set(Calendar.YEAR, 2015);
		
		Collection<StatisticData> inputData = Arrays.asList(
				new StatisticData("A", null, calendar2013.getTime(), 10 ),
				new StatisticData("B", null, calendar2015.getTime(), 50),
				new StatisticData("A", null, calendar2013.getTime(), 10)
				);
		
		Collection<StatisticData> actual = objectUnderTest.selectEntriesDesiredYear(inputData, calendar2013);
		
		Collection<StatisticData> expected = Arrays.asList(
				new StatisticData("A", null, calendar2013.getTime(), 10 ),
				new StatisticData("A", null, calendar2013.getTime(), 10)
				);
		
		assertEquals(expected, actual);
	}
	
	@Test
	void leastWordy() {
		Collection<StatisticData> data = Arrays.asList(
				new StatisticData("A", null, 10),
				new StatisticData("B", null, 50),
				new StatisticData("A", null, 10)
				);
		String actual = objectUnderTest.getLeastWordy(data);
		String expected = "A";
		
		assertEquals(expected, actual);
	}

	@Test
	void leastWordy_for_empty_input() {
		Collection<StatisticData> data = Arrays.asList();
		String actual = objectUnderTest.getLeastWordy(data);
		String expected = null;
		
		assertEquals(expected, actual);
	}
	
	@Test
	void leastWordy_scenario_1_with_null() {
		Collection<StatisticData> data = Arrays.asList(
				new StatisticData("A", null, null),
				new StatisticData("B", null, 50),
				new StatisticData("A", null, 10)
				);
		String actual = objectUnderTest.getLeastWordy(data);
		String expected = "A";
		
		assertEquals(expected, actual);
	}

	@Test
	void leastWordy_scenario_2_with_null() {
		Collection<StatisticData> data = Arrays.asList(
				new StatisticData("A", null, null),
				new StatisticData("B", null, 50),
				new StatisticData("A", null, null)
				);
		String actual = objectUnderTest.getLeastWordy(data);
		String expected = "A";
		
		assertEquals(expected, actual);
	}

	@Test
	void leastWordy_scenario_3_with_null() {
		Collection<StatisticData> data = Arrays.asList(
				new StatisticData("A", null, 10),
				new StatisticData("B", null, 10),
				new StatisticData("A", null, null)
				);
		String actual = objectUnderTest.getLeastWordy(data);
		String expected = null;
		
		assertEquals(expected, actual);
	}

	@Test
	void leastWordy_scenario_4_empty() {
		Collection<StatisticData> data = Arrays.asList(
				new StatisticData("A", null, null)
				);
		String actual = objectUnderTest.getLeastWordy(data);
		String expected = null;
		
		assertEquals(expected, actual);
	}
	
	@Test
	void getMostSpeeches_scenario_1_with_empty_input() {
		Collection<StatisticData> data = Arrays.asList();
		String actual = objectUnderTest.getMostSpeeches(data);
		String expected = null;
		
		assertEquals(expected, actual);
	}
	
	@Test
	void getMostSpeeches_scenario_1_with_null() {
		Collection<StatisticData> data = Arrays.asList(
				new StatisticData("A", null, 10),
				new StatisticData("B", "topic2", 20),
				new StatisticData("D", "topic1", 50),
				new StatisticData("D", "topic4", 50),
				new StatisticData("D", "topic5", 50)
				);
		String actual = objectUnderTest.getMostSpeeches(data);
		String expected = "D";
		
		assertEquals(expected, actual);
	}

	@Test
	void getMostSpeeches_scenario_1_Ambiguity() {
		Collection<StatisticData> data = Arrays.asList(
				new StatisticData("B", "topic2", 20),
				new StatisticData("D", "topic1", 50)
				);
		String actual = objectUnderTest.getMostSpeeches(data);
		String expected = null;
		
		assertEquals(expected, actual);
	}

	@Test
	void getMostSpeeches_scenario_2_Ambiguity() {
		Collection<StatisticData> data = Arrays.asList(
				new StatisticData("B", null, 20),
				new StatisticData("D", null, 50)
				);
		String actual = objectUnderTest.getMostSpeeches(data);
		String expected = null;
		
		assertEquals(expected, actual);
	}
	
	@Test
	void getMostSpeeches_scenario_3_Empty() {
		Collection<StatisticData> data = Arrays.asList(
				new StatisticData("B", null, 20)
				);
		String actual = objectUnderTest.getMostSpeeches(data);
		String expected = null;
		
		assertEquals(expected, actual);
	}

	@Test
	void getMostSpeeches() {
		Collection<StatisticData> data = Arrays.asList(
				new StatisticData("A", "topic1", 10),
				new StatisticData("B", "topic2", 20),
				new StatisticData("A", "topic3", 30),
				new StatisticData("C", "topic1", 40),
				new StatisticData("D", "topic1", 50),
				new StatisticData("D", "topic4", 50),
				new StatisticData("D", "topic5", 50)
				);
		String actual = objectUnderTest.getMostSpeeches(data);
		String expected = "D";
		
		assertEquals(expected, actual);
	}
	
	@Test
	void getMostSecurity() {
		Collection<StatisticData> data = Arrays.asList(
				new StatisticData("A", "topic1", 10),
				new StatisticData("B", "topic2", 20),
				new StatisticData("A", "topic3 Sicherheit", 30),
				new StatisticData("C", "topic1", 40),
				new StatisticData("D", "topic1", 50),
				new StatisticData("D", "topic4", 50),
				new StatisticData("D", "topic5", 50)
				);
		String actual = objectUnderTest.getMostSecurity(data);
		String expected = "A";
		
		assertEquals(expected, actual);
	}

	@Test
	void getMostSecurity_scenario_1_with_null() {
		Collection<StatisticData> data = Arrays.asList(
				new StatisticData("A", "topic1", 10),
				new StatisticData("B", "topic2", 20),
				new StatisticData("A", null, 30),
				new StatisticData("C", "topic1", 40),
				new StatisticData("D", "topic1", 50),
				new StatisticData("D", "topic4", 50),
				new StatisticData("D", "topic5", 50)
				);
		String actual = objectUnderTest.getMostSecurity(data);
		String expected = null;
		
		assertEquals(expected, actual);
	}

	@Test
	void getMostSecurity_scenario_1_Ambiguity() {
		Collection<StatisticData> data = Arrays.asList(
				new StatisticData("A", "topic1", 10),
				new StatisticData("B", "topic2", 20),
				new StatisticData("A", "topic3 Sicherheit", 30),
				new StatisticData("C", "topic1", 40),
				new StatisticData("D", "topic1 Sicherheit", 50),
				new StatisticData("D", "topic4", 50),
				new StatisticData("D", "topic5", 50)
				);
		String actual = objectUnderTest.getMostSecurity(data);
		String expected = null;
		
		assertEquals(expected, actual);
	}
	@Test
	void getMostSecurity_scenario_3_empty() {
		Collection<StatisticData> data = Arrays.asList(
				new StatisticData("A", "topic1", 10)
				);
		String actual = objectUnderTest.getMostSecurity(data);
		String expected = null;
		
		assertEquals(expected, actual);
	}

	@Test
	void getMostSecurity_scenario_4_empty_input() {
		Collection<StatisticData> data = Arrays.asList();
		String actual = objectUnderTest.getMostSecurity(data);
		String expected = null;
		
		assertEquals(expected, actual);
	}
}
