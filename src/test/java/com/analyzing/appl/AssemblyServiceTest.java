package com.analyzing.appl;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Date;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.analyzing.appl.domain.StatisticData;
import com.analyzing.appl.domain.dto.StatisticDataCsvDto;
import com.analyzing.appl.service.AssemblyService;

@SpringBootTest
class AssemblyServiceTest {
	@Autowired
	private AssemblyService objectUnderTest;
	
	@Test
	void assemblyDtoToDomainTest() {
		Integer wordCount = 10;
		Date date = new Date();
		String thema = "thema";
		String politician="abc";
		
		StatisticDataCsvDto csvDto = new StatisticDataCsvDto();
		csvDto.setDate(date);
		csvDto.setPolitician(politician);
		csvDto.setThema(thema);
		csvDto.setWordCount(wordCount);
		
		StatisticData actual = objectUnderTest.assemblyDtoToDomain(csvDto);
		StatisticData expected = new StatisticData(politician, thema, date, wordCount);
		
		assertEquals(expected, actual);
	}
}
