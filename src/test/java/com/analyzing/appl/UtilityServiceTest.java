package com.analyzing.appl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.analyzing.appl.service.UtilityService;

@SpringBootTest
class UtilityServiceTest {
	@Autowired
	private UtilityService objectUnderTest;
	
	@Test
	void extractUrlParameters_Scenario_3() {
		String stringUrlParam ="url1=ftp://pi@192.168.178.74/kgw?password=mypass&passiveMode=true&fileName=statistics&noop=true&url2=file:////home/admin/input";		
		
		Collection<String> actual = objectUnderTest.extractParameters(stringUrlParam);
		Collection<String> expected = new ArrayList<>();
		expected.add("ftp://pi@192.168.178.74/kgw?password=mypass&passiveMode=true&fileName=statistics&noop=true");
		expected.add("file:////home/admin/input");
		
		assertEquals(expected, actual);
		
	}
	
	@Test
	void checkUrlValidation_scenario_file_schema() {
		String stringFormUrl = "file:///home/admin/git/basex/README.md";
		boolean actual = objectUnderTest.isUrlValid(stringFormUrl);
		
		assertTrue(actual);
	}
	@Test
	void checkUrlValidation_scenario_http_schema() {
		String stringFormUrl = "http://www.example.com:8040/folder/exist?name=sky";
		boolean actual = objectUnderTest.isUrlValid(stringFormUrl);
		
		assertTrue(actual);
	}
	
	@Test
	void checkConversionToUrlCollection_Scenario_1() {
		Collection<String> inputData = new ArrayList<>();
		inputData.add("file:///home/admin/git/basex/README.md");
		
		try {
			Collection<URI> expected = new ArrayList<URI>();
			expected.add(new URI("file:///home/admin/git/basex/README.md"));
			
			Collection<URI> actual = objectUnderTest.conversionToUriCollection(inputData);
			assertEquals(expected, actual);
			
		} catch (URISyntaxException e) {
			// may be ignored
		}
	}

	@Test
	void checkConversionToUrlCollection_Scenario_2() {
		Collection<String> inputData = new ArrayList<>();
		inputData.add("ftp://pi@192.168.178.74/kgw?password=mypass&passiveMode=true&fileName=statistics&noop=true");
		
		try {
			Collection<URI> expected = new ArrayList<URI>();
			expected.add(new URI("ftp://pi@192.168.178.74/kgw?password=mypass&passiveMode=true&fileName=statistics&noop=true"));
			
			Collection<URI> actual = objectUnderTest.conversionToUriCollection(inputData);
			assertEquals(expected, actual);
			
		} catch ( URISyntaxException e) {
			// may be ignored
		}
	}
}
