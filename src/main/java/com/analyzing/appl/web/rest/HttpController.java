package com.analyzing.appl.web.rest;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.analyzing.appl.domain.AnalyseReport;
import com.analyzing.appl.domain.HttpResponse;
import com.analyzing.appl.domain.StatisticData;
import com.analyzing.appl.service.api.IDataRepositoryEmulationService;
import com.analyzing.appl.service.api.IStatisticAnalyserService;
import com.analyzing.appl.service.api.IUriDataLoadService;
import com.analyzing.appl.service.api.IUtilityService;

@RestController
public class HttpController {
	@Autowired
	private IUtilityService utilityService;
	@Autowired
	private IUriDataLoadService urlDataLoadService;
	@Autowired
	private IStatisticAnalyserService analyserService;
	@Autowired
	private IDataRepositoryEmulationService repository;
	
	private Collection<StatisticData> data;
	
	
	@PostConstruct
	private void init() {
		data = new ArrayList<>();
	}
	
	@GetMapping("/evaluation")
	public ResponseEntity<HttpResponse> getAnalyse(HttpServletRequest request){
		HttpResponse response;
		AnalyseReport analyseResponse;
		try {
			
			Collection<String> requestedUrlsStringForm = utilityService.extractParameters(request.getQueryString());
			Collection<URI> requestedUriCollection = utilityService.conversionToUriCollection(requestedUrlsStringForm);

			requestedUriCollection.stream()
			.forEach( uri -> urlDataLoadService.loadData(uri) );
			
			data = repository.getStatistics();
			analyseResponse = analyserService.processStatisticData(data);
			
			response = new HttpResponse();
			response.setStatus(HttpStatus.OK.value());
			response.setBody(analyseResponse);
			
			return ResponseEntity
					.ok()
					.body(response);
			
			
		} catch(Exception e) {
			response = new HttpResponse();
			response.setStatus(HttpStatus.CONFLICT.value());
			return ResponseEntity
					.status(HttpStatus.CONFLICT).body(response);
		}finally {
			repository.clean();
		}
	}
	@GetMapping("/")
	public ResponseEntity<String> processMiscRequests(){
		return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("wrong request");
	}
}
