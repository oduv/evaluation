package com.analyzing.appl.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;


@Component
@Aspect
public class ValidationAspect {
	private final Logger log = LoggerFactory.getLogger(getClass());

	@Before("execution(*  com.analyzing.appl.web.rest.*Controller*.*(..))")
	public void validateNotNullForControllers(JoinPoint joinPoint) {
		for(Object arg:joinPoint.getArgs()){
			if(arg==null && log.isErrorEnabled()){
				String msg = String.format("the argument %s of method %s must not be null.", arg, joinPoint.getSignature().getName());
				log.error(msg);
				Assert.notNull(arg, msg);
			}
		}
	}
	
	@Before("execution(*  com.analyzing.appl.service.*Service*.*(..))")
	public void validateNotNullForServices(JoinPoint joinPoint)  {
		validateNotNullForControllers(joinPoint);
	}
	
}
