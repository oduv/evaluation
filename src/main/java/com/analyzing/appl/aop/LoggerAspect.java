package com.analyzing.appl.aop;

import java.util.Arrays;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
@Aspect
public class LoggerAspect {
	private final Logger log = LoggerFactory.getLogger(getClass());

	@Around("execution( * com.analyzing.appl.web.rest.*Controller*.*(..))")
	public Object logAroundControllers(ProceedingJoinPoint joinPoint) throws Throwable {
		String methodName;
		String controllerName;
		methodName = joinPoint.getSignature().getName();
		controllerName = joinPoint.getSignature().getDeclaringType().getName();
		if(log.isInfoEnabled()) {
			log.info(String.format("called: %s.%s", controllerName,  methodName ));
		}
		
		if(log.isDebugEnabled()) {
			log.debug(String.format("START:: %s.%s", controllerName, methodName) );
			log.debug(joinPoint.getSignature().getDeclaringTypeName());
			log.debug(String.format("Arguments : %s", Arrays.toString(joinPoint.getArgs())));
		}

		Object result = joinPoint.proceed(); // continue on the intercepted
		
		if(log.isDebugEnabled()){
			log.debug(String.format("Returned value: [%s]", result));
			log.debug(String.format("END:: %s", methodName));
		}

		return result;
	}

	@Around("execution( * com.analyzing.appl.service.*Service*.*(..))")
	public Object logAroundServices(ProceedingJoinPoint joinPoint) throws Throwable {
		return logAroundControllers(joinPoint);
	}

}
