package com.analyzing.appl.service.api;

import java.net.URI;
import java.util.Collection;


public interface IUtilityService {
	/**
	 * to extract the parameter's collection
	 * @param request
	 */
	public Collection<String> extractParameters(String urlParameters );
	/**
	 * for validation the url
	 * @param stringUrl
	 * @return
	 */
	public boolean isUrlValid(String stringUrl);
	
	/**
	 * conversion the collection of string's form of URL-token into collection of URL-type objects
	 * @param inputData
	 * @return
	 */
	public Collection<URI> conversionToUriCollection(Collection<String> inputData);
}
