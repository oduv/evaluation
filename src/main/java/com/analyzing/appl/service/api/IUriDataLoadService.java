package com.analyzing.appl.service.api;

import java.net.URI;
import java.util.Collection;
import java.util.concurrent.CompletableFuture;

import com.analyzing.appl.domain.StatisticData;

public interface IUriDataLoadService {
	/**
	 * must connect to requested URI and read data
	 * @param theUri
	 * @return
	 */
	public CompletableFuture<Collection<StatisticData>> loadData(URI theUri);
}
