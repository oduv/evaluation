package com.analyzing.appl.service.api;

import java.util.Collection;

import com.analyzing.appl.domain.StatisticData;

public interface IDataRepositoryEmulationService {
	/**
	 * adds the statistic data into storage
	 * @param statisticData
	 */
	public void addStatisticData(StatisticData statisticData);
	/**
	 * cleans the data base between "session"
	 */
	public void clean();
	/**
	 * returns all data from "data base"
	 */
	public Collection<StatisticData> getStatistics();
}
