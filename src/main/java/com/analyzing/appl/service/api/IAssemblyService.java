package com.analyzing.appl.service.api;

import com.analyzing.appl.domain.StatisticData;
import com.analyzing.appl.domain.dto.StatisticDataCsvDto;

public interface IAssemblyService {
	/**
	 * conversion the dto into domain object
	 * @param dto
	 * @return
	 */
	public StatisticData assemblyDtoToDomain(StatisticDataCsvDto dto);
}
