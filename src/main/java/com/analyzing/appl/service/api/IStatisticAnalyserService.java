package com.analyzing.appl.service.api;

import java.util.Calendar;
import java.util.Collection;

import com.analyzing.appl.domain.AnalyseReport;
import com.analyzing.appl.domain.StatisticData;

public interface IStatisticAnalyserService {
	/**
	 * goes over collection of statistic data and produce the report in form of AnalyseResponse
	 * @param data
	 * @return
	 */
	public AnalyseReport processStatisticData(Collection<StatisticData> data);
	
	/**
	 * assist method to filter entries for desired year 
	 * @param data
	 * @param targetCalendar - calendar, which is tuned for targeted year
	 * @return	- filtered collection accordance to desired year
	 */
	public Collection<StatisticData> selectEntriesDesiredYear(Collection<StatisticData> data, Calendar targetCalendar);
	
	/**
	 * figures out the name of politician with minimum wordy
	 * @param data
	 * @return
	 */
	public String getLeastWordy(Collection<StatisticData> data);
	
	/**
	 * figures out the name of politician with most speeches (thema count ?)
	 * @param data
	 * @return
	 */
	public String getMostSpeeches(Collection<StatisticData> data);
	
	/**
	 * figures out the name of politician with most speeches (thema consist the word "Sicherheit" )
	 * @param data
	 * @return
	 */
	public String getMostSecurity(Collection<StatisticData> data);
}
