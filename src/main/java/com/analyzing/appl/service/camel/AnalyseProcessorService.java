package com.analyzing.appl.service.camel;

import java.util.Arrays;
import java.util.Collection;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.analyzing.appl.domain.StatisticData;
import com.analyzing.appl.domain.dto.StatisticDataCsvDto;
import com.analyzing.appl.service.api.IAssemblyService;
import com.analyzing.appl.service.api.IDataRepositoryEmulationService;

/**
 * reveals the csv's data and converse it into domain object
 * 
 * @author admin
 *
 */
@Service
public class AnalyseProcessorService implements Processor {
	private final Logger log = LoggerFactory.getLogger(getClass());

	@Autowired
	private IAssemblyService assemblyService;
	@Autowired
	private IDataRepositoryEmulationService dataRepository;

	private StatisticData statisticData;

	@Override
	public void process(Exchange exchange) throws Exception {
		Collection<StatisticDataCsvDto> csvContent;

		if (exchange.getIn().getBody() instanceof Collection) {
			csvContent = exchange.getIn().getBody(Collection.class);
		} else {
			csvContent = Arrays.asList(exchange.getIn().getBody(StatisticDataCsvDto.class));
		}

		csvContent.stream().forEach(statisticDataDto -> {
			if (log.isDebugEnabled()) {
				log.debug(String.format("found statistic data entry: %s", statisticDataDto));
			}
			statisticData = assemblyService.assemblyDtoToDomain(statisticDataDto);
			dataRepository.addStatisticData(statisticData);
		});

		if (log.isDebugEnabled()) {
			log.debug(String.format("total processed and added [%d] entries", csvContent.size()));
		}

	}
}
