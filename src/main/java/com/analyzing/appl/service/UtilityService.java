package com.analyzing.appl.service;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import javax.annotation.PostConstruct;

import org.apache.commons.validator.routines.UrlValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.analyzing.appl.service.api.IUtilityService;

@Service
public class UtilityService implements IUtilityService {
	private final Logger log = LoggerFactory.getLogger(UtilityService.class);
	private final String[] schemes = {"http","https", "ftp", "file"}; 
	private UrlValidator urlValidator;
	private String regExprPattern = "url[0-9]?=";

	@PostConstruct
	private void init() {
		urlValidator = new UrlValidator(schemes);
	}
	
	@Override
	public Collection<String> extractParameters(String urlParameters) {
		Collection<String> urls = new ArrayList<>();
		String[] tokens = urlParameters.split(regExprPattern);
		Arrays.asList(tokens).stream()
			.filter( token -> !token.isBlank() || !token.isEmpty())
			.forEach( token -> {
				if(token.endsWith("&")) {
					urls.add(token.substring(0, token.length() - 1));
				}else {
					urls.add(token);
				}
			});
		
		
		return urls;
	}
	
	/**
	 * validate the string's form of URL before create the real URL
	 * @param stringUrl
	 * @return
	 */
	public boolean isUrlValid(String stringUrl) {
		return urlValidator.isValid(stringUrl);
	}
	
	public Collection<URI> conversionToUriCollection(Collection<String> inputData){
		Collection<URI> urlCollection = new ArrayList<>();
		String msg = "The uri %s is not valid and can't be taken in account";
		for (String stringUri : inputData) {
			// probably the normalization of parameter will be a good idea, but not yet
			if(isUrlValid(stringUri)) {
				try {
					urlCollection.add(new URI(stringUri));
				} catch (URISyntaxException e) {
					log.error(String.format(msg,stringUri));
				}
			}else {
				if(log.isDebugEnabled()) {
					log.error(String.format(msg,stringUri));
				}
			}
			
		}
		return urlCollection;
	}

}
