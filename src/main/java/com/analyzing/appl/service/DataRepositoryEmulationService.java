package com.analyzing.appl.service;

import java.util.ArrayList;
import java.util.Collection;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Service;

import com.analyzing.appl.domain.StatisticData;
import com.analyzing.appl.service.api.IDataRepositoryEmulationService;

/**
 * The class plays as simulation of data base
 * @author admin
 *
 */
@Service
public class DataRepositoryEmulationService implements IDataRepositoryEmulationService {
	// emulation the storage (data base)
	// for simplicity took the collection instead of set 
	private Collection<StatisticData> storage;
	
	public DataRepositoryEmulationService() {
		// to prevent the overloading
	}
	
	@PostConstruct
	private void init() {
		storage = new ArrayList<>();
	}
	
	@Override
	public void addStatisticData(StatisticData statisticData) {
		storage.add(statisticData);
	}

	@Override
	public void clean() {
		storage.clear();
	}

	@Override
	public Collection<StatisticData> getStatistics() {
		return storage;
	}

}
