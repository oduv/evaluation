package com.analyzing.appl.service;

import org.springframework.stereotype.Service;

import com.analyzing.appl.domain.StatisticData;
import com.analyzing.appl.domain.dto.StatisticDataCsvDto;
import com.analyzing.appl.service.api.IAssemblyService;
@Service
public class AssemblyService implements IAssemblyService {

	/**
	 * conversion the dto-object into domain object
	 */
	@Override
	public StatisticData assemblyDtoToDomain(StatisticDataCsvDto dto) {
		StatisticData domainObject = new StatisticData();
		
		domainObject.setDate(dto.getDate());
		domainObject.setPolitician(dto.getPolitician());
		domainObject.setThema(dto.getThema());
		domainObject.setWordCount(dto.getWordCount());
		return domainObject;
	}

}
