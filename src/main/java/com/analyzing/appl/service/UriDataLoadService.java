package com.analyzing.appl.service;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

import javax.annotation.PostConstruct;

import org.apache.camel.CamelContext;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.dataformat.bindy.csv.BindyCsvDataFormat;
import org.apache.camel.spi.DataFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.analyzing.appl.domain.StatisticData;
import com.analyzing.appl.domain.dto.StatisticDataCsvDto;
import com.analyzing.appl.service.api.IDataRepositoryEmulationService;
import com.analyzing.appl.service.api.IUriDataLoadService;

@Service
public class UriDataLoadService implements IUriDataLoadService {
	private final Logger log = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private CamelContext camelContext;
	@Autowired
	private IDataRepositoryEmulationService dataRepository;
	@Autowired
	private Processor processor;
	@Autowired
	private ProducerTemplate producerTemplate;
	
	private Collection<StatisticData> data;
	private DataFormat dataFormat;

	public UriDataLoadService() {
		// to prevent the overloading
	}
	
	@PostConstruct
	private void init(){
		data = new ArrayList<>();
		dataFormat = new BindyCsvDataFormat(StatisticDataCsvDto.class);
	}
	
	@Override
	public CompletableFuture<Collection<StatisticData>> loadData(final URI theUri) {
		Future<?> future;
		try {
			// it is necessary to create a new instance of RouteBuilder each time, 
			// cause theUrl is changed by each iteration  
			camelContext.addRoutes(new RouteBuilder() {
				
				@Override
				public void configure() throws Exception {
					from("direct:start")
					.routeId("doJob")
					.pollEnrich(theUri.toString())
					.unmarshal(dataFormat)
					.process(processor)
					.end();
				}
			});
			
			future = producerTemplate.asyncSendBody("direct:start", theUri.toString());
			// waiting till previous operation will be completed
			future.get();
			
			data = dataRepository.getStatistics();
			return CompletableFuture.completedFuture(data);
		} catch (Exception e) {
			log.error("Please check your network performance");
			return CompletableFuture.failedFuture(e);
		}
	}
}
