package com.analyzing.appl.service;

import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.analyzing.appl.domain.AnalyseReport;
import com.analyzing.appl.domain.StatisticData;
import com.analyzing.appl.exceptions.AmbiguityException;
import com.analyzing.appl.service.api.IStatisticAnalyserService;

@Service
public class StatisticAnalyserService implements IStatisticAnalyserService {
	
	@Value("${com.analyser.appl.targetYear}")
	private int targetYear;
	
	@Override
	public AnalyseReport processStatisticData(Collection<StatisticData> rowData) {
		Calendar targetCalendar = Calendar.getInstance();
		targetCalendar.set(Calendar.YEAR, targetYear);
		
		Collection<StatisticData> data = this.selectEntriesDesiredYear(rowData, targetCalendar);
		
		AnalyseReport analyseResponse = new AnalyseReport();
		analyseResponse.setLeastWordy(this.getLeastWordy(data));
		analyseResponse.setMostSecurity(this.getMostSecurity(data));
		analyseResponse.setMostSpeeches(this.getMostSpeeches(data));
		
		
		return analyseResponse;
	}
	
	public Collection<StatisticData> selectEntriesDesiredYear(Collection<StatisticData> data, Calendar targetCalendar){
		// used to compute the year for static data entry
		Calendar tmpCalendar = Calendar.getInstance();
		
		return data.stream()
				.filter(stat -> {
					tmpCalendar.setTime(stat.getDate());
					return targetCalendar.get(Calendar.YEAR) == tmpCalendar.get(Calendar.YEAR);
				} )
				.collect(Collectors.toList());
	}
	
	public String getMostSecurity(Collection<StatisticData> data) {
		if(data.isEmpty()) {
			return null;
		}
		
		Map<String, Integer> mostSsecurity = new HashMap<>();
		data.stream()
		.forEach( stat -> {
			String key = stat.getPolitician();
			Integer value = mostSsecurity.get(key);
			if(value == null ) {
				value = 0;
			}
			
			if(	(stat.getThema() !=null && !stat.getThema().trim().isEmpty()) &&
				  stat.getThema().contains("Sicherheit") ) {
				value++;
			}
			mostSsecurity.put(key, value);
		});
		
		try {
			Optional<String> mostSecurityPol = mostSsecurity.keySet().stream()
					.max( (val1, val2) -> {
						Integer intVal1 = mostSsecurity.get(val1);
						Integer intVal2 = mostSsecurity.get(val2);
						if(intVal1.equals(intVal2) ) {
							throw new AmbiguityException() ;
						}
						return mostSsecurity.get(val1).compareTo(mostSsecurity.get(val2));
					});

			if(mostSsecurity.keySet().size() == 1 && Integer.parseInt(mostSsecurity.values().toArray()[0].toString()) == 0){
				return null;
			}
			
			return mostSecurityPol.isPresent() ? mostSecurityPol.get() : null ;
		}catch(AmbiguityException ae) {
			return null;
		}
	}
	
	public String getMostSpeeches(Collection<StatisticData> data) {
		if(data.isEmpty()) {
			return null;
		}
		
		Map<String, Integer> mostSpeeches = new HashMap<>();
		data.stream()
		 .forEach( stat -> {
			 	String key = stat.getPolitician();
				Integer value = mostSpeeches.get(key);
				if(value == null ) {
					value = 0;
				}
				
				
				if(stat.getThema() !=null && !stat.getThema().trim().isEmpty()) {
					value++;
				}
				mostSpeeches.put(key, value);
		 });
		
		try {
			Optional<String> mostSpeechesPol = mostSpeeches.keySet().stream()
					.max( (val1, val2) -> {
						Integer intVal1 = mostSpeeches.get(val1);
						Integer intVal2 = mostSpeeches.get(val2);
						if(intVal1.equals(intVal2) ) {
							throw new AmbiguityException() ;
						}
						return mostSpeeches.get(val1).compareTo(mostSpeeches.get(val2));
					});
			
			if(mostSpeeches.keySet().size() == 1 && Integer.parseInt(mostSpeeches.values().toArray()[0].toString()) == 0){
				return null;
			}
			
			return mostSpeechesPol.isPresent() ? mostSpeechesPol.get() : null ; 
			
		}catch(AmbiguityException ae) {
			return null;
		}
		
	}
	
	public String getLeastWordy(Collection<StatisticData> data) {
		if(data.isEmpty()) {
			return null;
		}
		
		Map<String, Integer> leastWordy = new HashMap<>();
		data.stream()
		 .forEach( stat -> {
			 	String key = stat.getPolitician();
				Integer value = leastWordy.get(key);
				if(value == null ) {
					value = 0;
				}
				
				if(stat.getWordCount() != null) {
					value += stat.getWordCount();
				}
				leastWordy.put(key, value);
		 });
		
		try {
			Optional<String> leastWordyPol = leastWordy.keySet().stream()
					.min( (val1, val2) -> {
						Integer intVal1 = leastWordy.get(val1);
						Integer intVal2 = leastWordy.get(val2);
						if(intVal1.equals(intVal2) ) {
							throw new AmbiguityException() ;
						}
						return leastWordy.get(val1).compareTo(leastWordy.get(val2));
					});
			
			if(leastWordy.keySet().size() == 1 && Integer.parseInt(leastWordy.values().toArray()[0].toString()) == 0){
				return null;
			}
			
			return leastWordyPol.isPresent() ? leastWordyPol.get() : null;
			
		}catch(AmbiguityException ae) {
			return null;
		}
	}

}
