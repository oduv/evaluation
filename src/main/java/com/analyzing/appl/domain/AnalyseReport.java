package com.analyzing.appl.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class AnalyseReport {
	private String mostSpeeches;
	private String mostSecurity;
	private String leastWordy;
	
	public AnalyseReport() {
		// to prevent the overloading
	}

	public AnalyseReport(String mostSpeeches, String mostSecurity, String leastWordy) {
		super();
		this.mostSpeeches = mostSpeeches;
		this.mostSecurity = mostSecurity;
		this.leastWordy = leastWordy;
	}
}
