package com.analyzing.appl.domain;

import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class StatisticData {
	private String politician;
	private String thema;
	private Date date;
	private Integer wordCount;
	
	public StatisticData() {
		// to prevent the overloading
	}

	public StatisticData(String politician, String thema, Integer wordCount) {
		this();
		this.politician = politician;
		this.thema = thema;
		this.wordCount = wordCount;
	}
	
	public StatisticData(String politician, String thema, Date date, Integer wordCount) {
		this(politician,thema,wordCount);
		this.date = date;
	}
}
