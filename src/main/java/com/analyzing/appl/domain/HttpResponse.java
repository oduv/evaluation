package com.analyzing.appl.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode
public class HttpResponse {
	private Integer status;
	private AnalyseReport body;
	
	public HttpResponse() {
		// to prevent the overloading
	}
}
