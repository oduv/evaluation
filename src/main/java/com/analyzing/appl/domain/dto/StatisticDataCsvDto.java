package com.analyzing.appl.domain.dto;

import java.util.Date;

import org.apache.camel.dataformat.bindy.annotation.CsvRecord;
import org.apache.camel.dataformat.bindy.annotation.DataField;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * The class is applied to mapping the csv-row 
 * @author admin
 *
 */
@Data
@EqualsAndHashCode
@CsvRecord(separator = ",", skipFirstLine = true, crlf = "UNIX")
public class StatisticDataCsvDto {
	@DataField(pos=1)
	private String politician;
	@DataField(pos=2)
	private String thema;
	@DataField(pos=3, trim=true, pattern = "yyyy-MM-dd")
	private Date date;
	@DataField(pos=4, trim=true )
	private Integer wordCount;
	
	public StatisticDataCsvDto() {
		// to prevent the overloading
	}
}
